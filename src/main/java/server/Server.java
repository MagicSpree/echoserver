package server;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class Server {

    private static final Logger log = Logger.getLogger(Server.class);

    private ServerSocket serverSocket;

    @SuppressWarnings("InfiniteLoopStatement")
    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            log.info("Сервер запущен");
            while (true) {
                Thread threadSocket = new ServerThread(serverSocket.accept());
                threadSocket.setDaemon(true);
                threadSocket.start();
            }

        } catch (IOException e) {
            log.error(e);
        } finally {
            stop();
        }

    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            log.error(e);
        }

    }


    private static class ServerThread extends Thread {

        Socket clientSocket;
        private PrintWriter out;
        private BufferedReader reader;

        public ServerThread(Socket socket) {
            this.clientSocket = socket;
        }

        @Override
        public void run() {
            try {
                log.info("Подключен клиент: " + clientSocket.getLocalAddress());
                initStreams();
                String inputLine;
                while ((inputLine = readMessageFromUser()) != null) {
                    log.info("Прочитана строка: " + inputLine);
                    String convertedString = getConvertedString(inputLine);
                    log.info("Преобразованная строка: " + convertedString);
                    sendMessageToUser(convertedString);
                }
                close();
                log.info("Клиент " + clientSocket.getLocalAddress() + " отключен");
            } catch (IOException e) {
                log.error(e);
            }
        }

        private void initStreams() throws IOException {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        }

        private String readMessageFromUser() throws IOException {
            return reader.readLine();
        }

        public void sendMessageToUser(String inputString) {
            out.println(inputString);
        }

        public void close() throws IOException {
            reader.close();
            out.close();
            clientSocket.close();
        }

        private String getConvertedString(String clientMessage) {
            return String.format("[%s]: %s", new Date(), clientMessage);
        }

    }
}
