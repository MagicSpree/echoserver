package client;

import java.io.IOException;

public class RunClient {

    private final static String IP_ADDR = "127.0.0.1";
    private final static int PORT = 8000;

    public static void main(String[] args) {
        Client client = new Client(IP_ADDR,PORT);
        try {
            if (client.isConnected()) {
                System.out.println("Ожидание ввода сообщения: ");
                client.startConversation();
            }
            System.out.println("Ошибка подключения к серверу.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
