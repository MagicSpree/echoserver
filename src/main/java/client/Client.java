package client;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

    private static final Logger log = Logger.getLogger(Client.class);

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader serverResult;
    private BufferedReader clientEnter;
    private boolean isConnected = false;


    public Client(String ip, int port) {
        startConnection(ip, port);
    }

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            serverResult = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            clientEnter = new BufferedReader(new InputStreamReader(System.in));
            log.info("Установлено соединение с сервером");
            isConnected = true;
        } catch (IOException e) {
            log.error(e);
        }
    }

    public void startConversation() throws IOException {
        while (!clientSocket.isClosed()) {
            String clientCommand = clientEnter.readLine();
            if (!clientCommand.isEmpty()) {
                sendMessage(clientCommand);
                log.info("Отправлено сообщение: " + clientCommand);
                String response = readMessageFromServer();
                if (response == null) {
                    log.info("Cервер был отключен!");
                    stopConnection();
                } else {
                    log.info("Получено сообщение от сервера :" + response);
                }
            }
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    private void sendMessage(String message) {
        out.println(message);
    }

    private String readMessageFromServer() throws IOException {
        return serverResult.readLine();
    }

    public void stopConnection() throws IOException {
        serverResult.close();
        clientEnter.close();
        out.close();
        clientSocket.close();
    }

}
